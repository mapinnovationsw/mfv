const createError = require('http-errors'),
    express = require('express'),
    path = require('path'),
    cookieParser = require('cookie-parser'),
    logger = require('morgan'),
    sassMiddleware = require('node-sass-middleware');

const indexRouter = require('./routes/index'),
    usersRouter = require('./routes/users');

const app = express();

app
.set('views', path.join(__dirname, 'views'))
.set('view engine', 'ejs');

app
.use(logger('tiny'))
.use(express.json())
.use(express.urlencoded({ extended: false }))
.use(cookieParser())
.use(sassMiddleware({
    src: path.join(__dirname, 'public/styles/scss'),
    dest: path.join(__dirname, 'public/styles/css'),
    indentedsyntax: false, // true = .sass and false = .scss
    sourcemap: false,
    prefix: '/styles/css',
    outputstyle: 'compressed',
    debug: false
}))
.use((request, response, next) => {
    response.setHeader('Access-Control-Allow-Origin', '*');
    response.setHeader('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE');
    response.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    response.setHeader('Access-Control-Allow-Credentials', true);
    next();
})
.use(express.static(path.join(__dirname, 'public')));

app
.use('/', indexRouter)
.use('/users', usersRouter);

app.use((req, res, next) => {
    next(createError(404));
});

app.use((err, req, res, next) => {
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    res.status(err.status || 500).render('error');
});

module.exports = app;