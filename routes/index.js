const express = require('express'),
    axios = require('axios'),
    router = express.Router();
//http://192.168.0.35:3000/?token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1MzYwODE2NTIsImlzcyI6Ik1BUFRFQ0hOT0xPR1kiLCJsb2dpbiI6Im1hcCJ9.DLMptJD_Zhl1Mv3Aq22wsx72lXb_2r0t7gvpKhMcJ0U&dtReferencia=03/09/2018&idTurno=4&filtroOp=0&cdGt=Atlas&turnoAtual=true
router
.get('/', (req, res, next) => {
    axios({
        method: 'POST',
        url: 'http://localhost:8080/idw/rest/monitorizacao',
        headers: {
            Authentication: req.query.token
        },
        data: {
            dtReferencia: req.query.dtReferencia,
            idTurno: parseInt(req.query.idTurno),
            filtroOp: parseInt(req.query.filtroOp),
            cdGt: req.query.cdGt,
            turnoAtual: JSON.parse(req.query.turnoAtual),
        },
        responseType: 'json'
    })
    .then(response => {
        const lstGtsPts = [];
        for (const i in response.data.pts) {
            lstGtsPts.push(response.data.pts[i]);
        }
        for (const i in response.data.gts) {
            lstGtsPts.push(response.data.gts[i]);
        }
        console.log(lstGtsPts);
        res.render('index', {title: 'MFV', pts: JSON.stringify(lstGtsPts)});
    })
    .catch(error => res.render('error', { message: error.message }));
})
.get('/teste', (request, response, next) => {
    axios.get('http://192.168.0.35:8000/json/monitorizacao.json')
    .then(data => response.render('index', {title: 'MFV', pts: JSON.stringify(data.data.pts)}))
    .catch(error => console.log(error));
})
.post('/salvar', (request, response, next) => {
    console.log(request.body);
    
    // fs.writeFile('/mfv/lista.json', request.body, err => {
    //     if(err) throw err;
        response.status(200).json({data: request.body});
    // });
});

module.exports = router;
