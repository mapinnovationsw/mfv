const express = require('express'),
    router = express.Router(),
    Promise = require('bluebird');

router
.get('/', (request, response, next) => {
    Promise
    .resolve()
    .then(() => retrieveVF())
    .then(response => {
        console.log(response);
    })
    .catch(err => err instanceof Error ? response.status(400).send('General Error') : response.status(200).json({err}));
});

module.exports = router;