labelText = new Konva.Text({
    x: 500,
    y: 25,
    fontFamily: 'Calibri',
    fontSize: 20,
    text: 'Add texto',
    fill: 'black',
});

labelText.on('click', function(){
    var label = prompt('Nome do label');
    drawText(label);
});

layer.add(labelText);
stage.add(layer);

function drawText(text) {
    textLabel = new Konva.Text({
        x: 500,
        y: 50,
        fontFamily: 'Calibri',
        fontSize: 20,
        text: text,
        fill: 'black',
        draggable: true
    });
    
    layer.add(textLabel);
    stage.add(layer);
}