arrow[0].onload = function() {
    drawArrow0(this, 'Fluxo De Informação', 150 * 1);
};

layer.draw();

stage.on('click', function (e) {
    // if click on empty area - remove all transformers
    if (e.target === stage) {
        stage.find('Transformer').destroy();
        layer.draw();
        return;
    }
    // do nothing if clicked NOT on our rectangles
    if (!e.target.hasName('rect'))
        return;
    // remove old transformers
    // TODO: we can skip it if current rect is already selected
    stage.find('Transformer').destroy();

    // create new transformer
    var tr = new Konva.Transformer();
    layer.add(tr);
    tr.attachTo(e.target);
    layer.draw();
});

function drawArrow0(image, name, n) {
    arrow0 = new Konva.Image({
        image: image,
        x: 50 + n,
        y: 10,
        width: 50,
        height: 50,
        draggable: true,
        name: name
    });

    arrow0
        .on('dragstart', function () {
            arrow0.stopDrag();
            var cloneArrow0 = arrow0.clone({
                x: this.x(),
                y: this.y(),
                name: 'rect',
                listening: true
            });

            cloneArrow0.off('dragstart');
            layer.add(cloneArrow0);
            cloneArrow0.startDrag();
        });

    layer.add(arrow0);
    stage.add(layer);
}