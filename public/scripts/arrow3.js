arrow[3].onload = function () {
    drawArrow3(this, 'Seta de Empurrar', 340 * 1);
};

layer.draw();

stage.on('click', function (e) {
    // if click on empty area - remove all transformers
    if (e.target === stage) {
        stage.find('Transformer').destroy();
        layer.draw();
        return;
    }
    // do nothing if clicked NOT on our rectangles
    if (!e.target.hasName('rect'))
        return;
    // remove old transformers
    // TODO: we can skip it if current rect is already selected
    stage.find('Transformer').destroy();

    // create new transformer
    var tr = new Konva.Transformer();
    layer.add(tr);
    tr.attachTo(e.target);
    layer.draw();
});

function drawArrow3(image, name, n) {
    arrow3 = new Konva.Image({
        image: image,
        x: 50 + n,
        y: 10,
        width: 50,
        height: 50,
        draggable: true,
        name: name
    });

    arrow3
        .on('dragstart', function () {
            arrow3.stopDrag();
            var cloneArrow3 = arrow3.clone({
                x: this.x(),
                y: this.y(),
                name: 'rect',
                listening: true
            });

            cloneArrow3.off('dragstart');
            layer.add(cloneArrow3);
            cloneArrow3.startDrag();
        });

    layer.add(arrow3);
    stage.add(layer);
}