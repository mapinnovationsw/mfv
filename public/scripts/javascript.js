var stage = new Konva.Stage({
        container: 'container',
        width: window.innerWidth,
        height: window.innerHeight
    }),
    layer = new Konva.Layer(),
    tooltipLayer = new Konva.Layer(),
    listImagens = [
       'Caixa de Dados',
       'Centro de Controle',
       'Cross Dock copy',
       'Cross Dock',
       'Empresa',
       'Estoque',
    //    'Fluxo De Informação_1',
    //    'Fluxo De Informação_2',
    //    'Fluxo de Informação',
       'InfoMobile',
       'Informações',
       'Kanban de Sinalização',
       'Milk Run',
       'Operador - Icone Geral'
    ],
    listImagensTwo = [
        'Pedidos',
        'Posto de Kanban',
        // 'posto',
        'Primeiro a entrar',
        'Processo de Produção',
        // 'Produtos acabados para o cliente',
        'Retirada',
        // 'Seta de Empurrar',
        'Supermercado',
        'Transporte Aéreo',
        'Transporte Ferroviário',
        'Transporte Fluvial',
        'Transporte Rodoviário',
        'Transporte Urgente'
    ],
    ptTooltip = new Konva.Text({
        text: '',
        fontFamily: 'Calibri',
        fontSize: 18,
        padding: 5,
        textFill: 'white',
        fill: 'black',
        alpha: 2,
        visible: false
    }),
    arrow = new Array();

arrow[0] = new Image();
arrow[1] = new Image();
arrow[2] = new Image();
arrow[3] = new Image();
arrow[0].src = 'images/Fluxo De Informação_1.png';
arrow[1].src = 'images/Fluxo de Informação.png';
arrow[2].src = 'images/Produtos acabados para o cliente.png';
arrow[3].src = 'images/Seta de Empurrar.png';
    
listImagens.forEach((imagem, i) => showImage(layer, imagem, i, 30, 50, 60));
listImagensTwo.forEach((imagem, i) => showImage(layer, imagem, i, 90, 50, 60));
    
function drawSetaEmpurrar(image, n) {
    var setaImage = new Konva.Image({
        image: image,
        x: 400 + n,
        y: 225,
        width: 50,
        height: 50,
        draggable: true
    });

    layer.add(setaImage);
    stage.add(layer);
}

function drawTeste(image, pt, n) {
    var ptImage = new Konva.Image({
        image: image,
        x: 300 + n,
        y: 200,
        width: 100,
        height: 100,
        draggable: true,
        // scale: {
        //     x: 1.5,
        //     y: 1.5
        // },
        name: construirPtGt(pt)
    });

    ptImage
    .on('mousemove', function() {
        var mouse = stage.getPointerPosition();
        ptTooltip
        .position({
            x: mouse.x + 25,
            y: mouse.y + 25,
        });
        ptTooltip.text(ptImage.name());
        ptTooltip.show();
        tooltipLayer.batchDraw();
    })
    .on('mouseout', function(){
        ptTooltip.hide();
        tooltipLayer.draw();
    });
    layer.add(ptImage);
    stage.add(layer);
}
    
tooltipLayer.add(ptTooltip);
stage.add(tooltipLayer);

var imagemObj = new Image();
var setaImagemObj = new Image();

imagemObj.onload = function() {
    pts.forEach((pt, i) => {
        drawTeste(this, pt, 150 * i);
    });
};

setaImagemObj.onload = function() {
    pts.forEach((pt, i) => {
        console.log(i);
        if (i != pts.length)
            drawSetaEmpurrar(this, 150 * i);
    });
};

function construirPtGt(pt) {
    if (pt.hasOwnProperty('cdPt')) {
        return `
            ${pt.cdPt}
            Descrição: ${pt.dsPt}    
            Turno: ${pt.turno}    
            Produto: ${pt.dsProduto}    
            Eficiência de Realização: ${pt.indicadores.eficienciaRealizacao}
            Eficiência de Ciclo: ${pt.indicadores.eficienciaCiclo}
            Índice de Refugo: ${pt.indicadores.indiceRefugo}
            Índice de Parada: ${pt.indicadores.indiceParada}
        `;
    } else {
        return `
            ${pt.cdGt}
            Descrição: ${pt.dsGt}  
            Indice OEE: ${pt.indiceOEE}    
            Indice OEE Meta: ${pt.indiceOEEMeta}
            Quantidade de Postos: ${pt.quantidadePostos}
        `;
    }
}
setaImagemObj.src = '/images/Seta de Empurrar.png';
imagemObj.src = '/images/posto.png';
