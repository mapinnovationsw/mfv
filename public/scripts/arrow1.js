arrow[1].onload = function () {
    drawArrow1(this, 'Fluxo de Informação', 210 * 1);
};

layer.draw();

stage.on('click', function (e) {
    // if click on empty area - remove all transformers
    if (e.target === stage) {
        stage.find('Transformer').destroy();
        layer.draw();
        return;
    }
    // do nothing if clicked NOT on our rectangles
    if (!e.target.hasName('rect'))
        return;
    // remove old transformers
    // TODO: we can skip it if current rect is already selected
    stage.find('Transformer').destroy();

    // create new transformer
    var tr = new Konva.Transformer();
    layer.add(tr);
    tr.attachTo(e.target);
    layer.draw();
});

function drawArrow1(image, name, n) {
    arrow1 = new Konva.Image({
        image: image,
        x: 50 + n,
        y: 10,
        width: 50,
        height: 50,
        draggable: true,
        name: name
    });

    arrow1
        .on('dragstart', function () {
            arrow1.stopDrag();
            var cloneArrow1 = arrow1.clone({
                x: this.x(),
                y: this.y(),
                name: 'rect',
                listening: true
            });

            cloneArrow1.off('dragstart');
            layer.add(cloneArrow1);
            cloneArrow1.startDrag();
        });

    layer.add(arrow1);
    stage.add(layer);
}