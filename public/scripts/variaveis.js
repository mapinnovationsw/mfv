var j = 0,
    pts = JSON.parse($('#pts').val()),
    lista = [],
    modal = document.getElementById('myModal'),
    btn = document.getElementById('myBtn'),
    span = document.getElementsByClassName('close')[0],
    text = new Konva.Text({
        x: 10,
        y: 0,
        fontFamily: 'Calibri',
        fontSize: 20,
        text: '',
        fill: 'black'
    }),
    tooltip = new Konva.Text({
        text: '',
        fontFamily: 'Calibri',
        fontSize: 30,
        padding: 5,
        textFill: 'white',
        fill: 'black',
        alpha: 0.75,
        visible: false,
        name: ''
    }),
    numEvents = 0,
    itemUp = null,
    lastX = 0,
    lastY = 0,
    markeditem = '';

btn.onclick = () => modal.style.display = 'block';
span.onclick = () => modal.style.display = 'none';

window.onclick = (e) => {
    if (e.target == modal)
        modal.style.display = 'none';
};

document.addEventListener('keyup', (e) => {
    if(itemUp != null){
        if (e.keyCode == 46) {
            var answer = confirm('Deseja apagar o item ' + itemUp.name());
            if (answer) {
                itemUp.destroy();
                layer.draw();
            }
        }
    }
});

$('#salvar').on('click', () => {
    console.log(stage.toJSON());
    // $.post('http://localhost:3000/salvar', {data: JSON.stringify(lista)}, function(data, status) {
    //     if(status == 'success')
    //         alert('JSON criado com sucesso!');
    // });
});

function showImage(layer, nome, medida, padding, tamanho, fator){
    var imagePattern = new Konva.Image({
            width: 50,
            height: tamanho
        }),
        heightOp = (20+(medida*fator)),
        imageGroup = new Konva.Group({
            x:padding,
            y: heightOp,    
            width : 30,
            height :30,    
            shadowBlur : 20,
            draggable : true,
            name: nome
        }),
        imageObj1 = new Image();

    imageObj1.onload = () => {
        imagePattern.image(imageObj1);
        layer.draw();
    };
    imageObj1.src = 'images/' + nome + '.png';
        
    imageGroup.add(imagePattern);
    layer.add(imageGroup);     
            
    imageGroup
    .on('mouseout', () => {
        itemUp = null;
        writeMessage('');
        tooltip.hide();
        tooltipLayer.draw();
    })
    .on('mouseover', function () {
        itemUp = this;
        writeMessage(this.name());
    })
    .on('mousemove',function(){
        if(!this.name().indexOf('posto')){
            var mousePos = stage.getPointerPosition();
            tooltip
            .position({
                x : mousePos.x + 5,
                y : mousePos.y + 5
            });
            tooltip.text(this.name())
            tooltip.show();
            tooltipLayer.batchDraw();
        }
    });

    layer.add(text);
    tooltipLayer.add(tooltip);
           
    imageGroup
    .on('dragstart', function() {
        imageGroup.stopDrag();
        var clone = imageGroup.clone({
                x: this.x(),
                y: this.y(),
                scale: {
                    x: 1.5,
                    y: 1.5
                },
                name: this.name() + j,
                listening: true
            });
        lista.push({name: this.name() + j, x: this.getX(), y: this.getY()});
        j++;
        
        clone.off('dragstart');
        layer.add(clone);
        clone.startDrag();
    })
    .on('dragend', function() {
        lista.forEach((imagem, i) => {
            if(imagem.name == this.name() && (this.getX() != imagem.x || this.getY() != imagem.x)) {
                lista[i].x = this.getX();
                lista[i].y = this.getY();
            }
        });
        if (!this.name().indexOf('Fluxo De Informação_1'))
            modal.style.display = 'block';
    })
    .on('dblclick', function() {
        if(lastX != 0){
            writeMessage('double click');
            var person = prompt('Alterar Nome da Variável');
            if (person != null)
                this.name(person) ;
        }
    });
}

function adjustPoint(e){
    var p=[lastX.getX(), lastY.getY(), circleA.getX(), circleA.getY()];
    arrow.setPoints(p);
    layer.draw();
}

function writeMessage(message) {
    text.setText(message);
    layer.draw();
}