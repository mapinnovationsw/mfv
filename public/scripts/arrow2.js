arrow[2].onload = function () {
    drawArrow2(this, 'Produtos acabados para o cliente', 270 * 1);
};

layer.draw();

stage.on('click', function (e) {
    // if click on empty area - remove all transformers
    if (e.target === stage) {
        stage.find('Transformer').destroy();
        layer.draw();
        return;
    }
    // do nothing if clicked NOT on our rectangles
    if (!e.target.hasName('rect'))
        return;
    // remove old transformers
    // TODO: we can skip it if current rect is already selected
    stage.find('Transformer').destroy();

    // create new transformer
    var tr = new Konva.Transformer();
    layer.add(tr);
    tr.attachTo(e.target);
    layer.draw();
});

function drawArrow2(image, name, n) {
    arrow2 = new Konva.Image({
        image: image,
        x: 50 + n,
        y: 10,
        width: 50,
        height: 50,
        draggable: true,
        name: name
    });

    arrow2
        .on('dragstart', function () {
            arrow2.stopDrag();
            var cloneArrow2 = arrow2.clone({
                x: this.x(),
                y: this.y(),
                name: 'rect',
                listening: true
            });

            cloneArrow2.off('dragstart');
            layer.add(cloneArrow2);
            cloneArrow2.startDrag();
        });

    layer.add(arrow2);
    stage.add(layer);
}